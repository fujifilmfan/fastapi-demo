from datetime import datetime, timezone

from sqlalchemy.orm import Session

from app import models
from app.models import Show, User


def get_user(db: Session, user_id: int) -> models.User:
    """Get user by ID.

    :param db: sqlalchemy.orm.sessionmaker object
    :param user_id: int
    :return: models.User object
    """

    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str) -> models.User:
    """Get user by email address.

    :param db: sqlalchemy.orm.sessionmaker object
    :param email: str
    :return: models.User object
    """

    return db.query(User).filter(User.email == email).first()


def get_users(db: Session, skip: int = 0, limit: int = 100) -> models.User:
    """Get users.

    :param db: sqlalchemy.orm.sessionmaker object
    :param skip: int
    :param limit: int
    :return: models.User objects (list?)
    """

    return db.query(User).offset(skip).limit(limit).all()


def create_user(db: Session, email: str, name: str) -> models.User:
    """Add a user to the database.

    :param db: sqlalchemy.orm.sessionmaker object
    :param email: str
    :param name: str
    :return: models.User object
    """

    now = datetime.now(timezone.utc)
    db_user = models.User(email=email, name=name, timestamp=now)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    return db_user


def like_show(db: Session, db_user: User, db_show: Show) -> models.User:
    db_user.shows.append(db_show)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def unlike_show(db: Session, db_user: User, db_show: Show) -> models.User:
    db_user.shows.remove(db_show)
    db.commit()
    db.refresh(db_user)
    return db_user
