from sqlalchemy.orm import Session

from app import models


def get_show(db: Session, show_id: int) -> models.Show:
    """Get TV show by ID.

    :param db: sqlalchemy.orm.sessionmaker object
    :param show_id: int
    :return: models.Show object
    """

    return db.query(models.Show).filter(models.Show.id == show_id).first()


def get_show_by_title(db: Session, title: str) -> models.Show:
    """Get TV show by title.

    :param db: sqlalchemy.orm.sessionmaker object
    :param title: str
    :return: models.Show object
    """

    return db.query(models.Show).filter(models.Show.title == title).first()


def get_shows(db: Session, skip: int = 0, limit: int = 100) -> models.Show:
    """Get TV shows.

    :param db: sqlalchemy.orm.sessionmaker object
    :param skip: int
    :param limit: int
    :return: models.Show objects (list?)
    """

    return db.query(models.Show).offset(skip).limit(limit).all()


def create_show(db: Session, title: str, year: int) -> models.Show:
    """Add a TV show to the database.

    :param db: sqlalchemy.orm.sessionmaker object
    :param title: str
    :param year: int
    :return: models.Show object
    """

    db_show = models.Show(title=title, year=year)
    db.add(db_show)
    db.commit()
    db.refresh(db_show)

    return db_show
