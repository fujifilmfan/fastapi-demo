## Contents
* [Introduction](#introduction)
* [Framework](#framework)
* [Running tests](#running-tests)
* [Adding tests](#adding-tests)


## Introduction
If you made some changes to the API, run some quick tests to check that things still work as expected.  

What I mean by a "unit test" is very well defined by the bullet points on 
[They're Called Microtests](https://anarchycreek.com/2009/05/20/theyre-called-microtests/).  

## Framework

### Unit test directory contents
As of 2022-03-23.  

```shell
.
├── .coveragerc
├── app
│   └── tests
│       ├── __init__.py
│       ├── pytest.ini
│       └── unit
│           ├── __init__.py
│           ├── fixtures
│           │   └── endpoints_fixtures.py
│           └── test_endpoints.py
└── pytest.ini
```

### Key unit testing items
* `.coveragerc` - tells **pytest** to omit certain files and directories from being tested   
* `tests/unit/` holds tests and static data used by the tests; specifically, it contains:
    * `conftest.py` - **pytest** fixtures, primarily mocks of various functions  
    * `fixtures/` - data used by the tests as function inputs and expected outputs  
## Running tests
From the project root, run `pytest app/tests/unit`.  That's it.  

```shell
> pytest app/tests/unit
pytest app/tests/unit
======================================================= test session starts ========================================================
platform darwin -- Python 3.9.11, pytest-7.1.1, pluggy-1.0.0 -- /Users/acetone/code/projects/fastapi-demo/.venv/bin/python
cachedir: .pytest_cache
rootdir: /Users/acetone/code/projects/fastapi-demo/app/tests, configfile: pytest.ini
plugins: anyio-3.5.0, mock-3.7.0, cov-3.0.0
collected 10 items

app/tests/unit/test_endpoints.py::test_create_show[valid_params] PASSED                                                      [ 10%]
app/tests/unit/test_endpoints.py::test_create_show[valid_params-year_string] PASSED                                          [ 20%]
app/tests/unit/test_endpoints.py::test_create_show_exceptions[show_exists] PASSED                                            [ 30%]
app/tests/unit/test_endpoints.py::test_create_show_exceptions[year_is_invalid_string] PASSED                                 [ 40%]
app/tests/unit/test_endpoints.py::test_create_show_exceptions[year_2_digits] PASSED                                          [ 50%]
app/tests/unit/test_endpoints.py::test_read_shows PASSED                                                                     [ 60%]
app/tests/unit/test_endpoints.py::test_read_show PASSED                                                                      [ 70%]
app/tests/unit/test_endpoints.py::test_create_user[valid_params] PASSED                                                      [ 80%]
app/tests/unit/test_endpoints.py::test_create_user_exceptions[user_exists] PASSED                                            [ 90%]
app/tests/unit/test_endpoints.py::test_read_users PASSED                                                                     [100%]

======================================================== 10 passed in 0.63s ========================================================
```

### Verbosity
If you want more verbose output, add the `-v` argument. For quieter output, add `-q`.  

Pytest has several degrees of verbosity, so commands like `pytest -vv` and `pytest -qqq`are also valid depending on the default verbosity, which can be changed in `pytest.ini` files.  

Here is an example using `pytest -q tests/unit`:  
```
$ pytest -q app/tests/unit
======================================================= test session starts ========================================================
platform darwin -- Python 3.9.11, pytest-7.1.1, pluggy-1.0.0
rootdir: /Users/acetone/code/projects/fastapi-demo/app/tests, configfile: pytest.ini
plugins: anyio-3.5.0, mock-3.7.0, cov-3.0.0
collected 10 items

app/tests/unit/test_endpoints.py ..........                                                                                  [100%]

======================================================== 10 passed in 0.43s ========================================================
```

Here is an example using `pytest -qq`:  
```
> pytest -qq app/tests/unit
pytest -qq app/tests/unit
..........                                                                                                                   [100%]
10 passed in 0.42s
```

### Troubleshooting
When trying to determine why a test is a failing, it can be helpful to run something like `pytest -vv -rsx -k test_census_bins` where  

* `-vv` shows extra [verbosity](#verbosity)  
* `-r` shows extra test summary information, like (f)ailed, (E)rror, or (s)kipped  
* `-s` (or `--capture-no`) allows standard output to the console (like from a print statement)  
* `-x` stops the testing after the first failure  
* `-k test_census_bins` runs only tests matching the expression (usually a test or file name) so you can narrow your focus to the failing test or group of tests  

### Coverage
"Untested code is broken code." ~Philipp von Weitershausen and Martin Aspeli

We don't need 100% code coverage - some statements are so simple that tests are unnecessary, for instance - but the goal is to cover all non-trivial functionality in the code base.

We can get a code coverage report using the **pytest-cov** module with `pytest --cov-report=term-missing --cov="." tests/unit` where  

* `--cov-report=term-missing` (or `--cov-report term-missing`) indicates that the type of report to generate is term-missing; term-missing shows line numbers of statements that lack test coverage  
* `--cov=.` (or `--cov .` or `--cov=censusapi`) specifies the path or package name to measure during execution  

Here is example output:  

```shell
======================================================= test session starts ========================================================
platform darwin -- Python 3.9.11, pytest-7.1.1, pluggy-1.0.0
rootdir: /Users/acetone/code/projects/fastapi-demo, configfile: pytest.ini
plugins: anyio-3.5.0, mock-3.7.0, cov-3.0.0
collected 10 items

app/tests/unit/test_endpoints.py ..........                                                                                  [100%]

---------- coverage: platform darwin, python 3.9.11-final-0 ----------
Name                                            Stmts   Miss  Cover   Missing
-----------------------------------------------------------------------------
app/__init__.py                                     0      0   100%
app/api/__init__.py                                 0      0   100%
app/api/api_v1/__init__.py                          0      0   100%
app/api/api_v1/endpoints/__init__.py                0      0   100%
app/api/api_v1/endpoints/shows.py                  28      2    93%   61, 99
app/api/api_v1/endpoints/users.py                  42     21    50%   48, 73-86, 91-104, 117-123
app/crud/__init__.py                                0      0   100%
app/crud/crud_show.py                              14      8    43%   14, 25, 37, 49-54
app/crud/crud_user.py                              26     17    35%   15, 26, 38, 50-55, 59-63, 67-70
app/db/__init__.py                                  0      0   100%
app/db/base.py                                     17      4    76%   28-32
app/db/sample_data.py                              23     23     0%   1-69
app/main.py                                        11     11     0%   1-18
app/models.py                                      17      0   100%
app/schemas.py                                     26      0   100%
app/server.py                                       2      2     0%   1-2
app/tests/__init__.py                               0      0   100%
app/tests/integration/__init__.py                   0      0   100%
app/tests/unit/__init__.py                          0      0   100%
app/tests/unit/fixtures/endpoints_fixtures.py      10      0   100%
app/tests/unit/test_endpoints.py                   59      0   100%
-----------------------------------------------------------------------------
TOTAL                                             275     88    68%


======================================================== 10 passed in 0.80s ========================================================```
```

## Adding tests
If you add or change functionality, add or change tests.  

If you break tests, such as when refactoring, fix the tests.  

When writing new unit tests (or refactoring old ones), write long, descriptive names for the test functions so that the source of any bug or failure is more readily apparent.  
