from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import models, schemas
from app.crud import crud_show, crud_user
from app.db.base import get_db


router = APIRouter(
    prefix='/users',
    tags=['users'],
    responses={
        200: {'description': 'OK'},
        400: {'description': 'Bad request'},
        404: {'description': 'Not found'},
    },
)


@router.post('/', response_model=schemas.UserSchema)
def create_user(
        email: str,
        name: str,
        db: Session = Depends(get_db),
) -> models.User:
    """Add a user to the database.

    **email**: string
    **name**: string

    \f
    FastAPI converts the models.User returned from crud_user.create_user()
    to schemas.UserSchema.

    :param email: str
    :param name: int
    :param db: sqlalchemy.orm.sessionmaker object
    :return: models.User object
    """

    db_user = crud_user.get_user_by_email(db, email=email)
    if db_user:
        raise HTTPException(
            status_code=400,
            detail='User with email address already exists.',
        )

    return crud_user.create_user(db=db, email=email, name=name)


@router.get('/', response_model=list[schemas.UserSchema])
def read_users(
        skip: int = 0,
        limit: int = 10,
        db: Session = Depends(get_db)
) -> list:
    """Get users.

    \f
    :param skip: int
    :param limit: int
    :param db: sqlalchemy.orm.sessionmaker object
    :return: list; models.User objects
    """

    users = crud_user.get_users(db, skip=skip, limit=limit)

    return users


@router.put('/{user_id}/like_show', response_model=schemas.UserSchema)
def like_show(user_id: int, show_id: int, db: Session = Depends(get_db)):
    if show_id:
        db_show = crud_show.get_show(db, show_id=show_id)
        db_user = crud_user.get_user(db, user_id=user_id)
        if db_show is None:
            raise HTTPException(
                status_code=404,
                detail='Show not found',
            )
        if db_user is None:
            raise HTTPException(
                status_code=404,
                detail='User not found',
            )
        return crud_user.like_show(db, db_user=db_user, db_show=db_show)


@router.delete('/{user_id}/unlike_show', response_model=schemas.UserSchema)
def unlike_show(user_id: int, show_id: int, db: Session = Depends(get_db)):
    if show_id:
        db_show = crud_show.get_show(db, show_id=show_id)
        db_user = crud_user.get_user(db, user_id=user_id)
        if db_show is None:
            raise HTTPException(
                status_code=404,
                detail='Show not found',
            )
        if db_user is None:
            raise HTTPException(
                status_code=404,
                detail='User not found',
            )
        return crud_user.unlike_show(db, db_user=db_user, db_show=db_show)


@router.get('/{user_id}', response_model=schemas.UserSchema)
def read_user(user_id: int, db: Session = Depends(get_db)) -> models.User:
    """Get user by ID.

    \f
    :param user_id: int
    :param db: sqlalchemy.orm.sessionmaker object
    :return: models.User object
    """

    db_user = crud_user.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(
            status_code=404,
            detail='User not found',
        )
    return db_user
