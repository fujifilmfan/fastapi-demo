from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import models, schemas
from app.crud import crud_show, crud_user
from app.db.base import get_db


router = APIRouter(
    prefix='/shows',
    tags=['shows'],
    responses={
        200: {'description': 'OK'},
        400: {'description': 'Bad request'},
        404: {'description': 'Not found'},
    },
)

@router.post('/', response_model=schemas.ShowSchema)
def create_show(
        title: str,
        year: int,
        db: Session = Depends(get_db),
) -> models.Show:
    """Add a show to the database.

    **title**: string
    **year**: integer; four-digit year

    \f
    FastAPI converts the models.Show returned from crud_show.create_show()
    to schemas.ShowSchema.

    :param title: str
    :param year: int
    :param db: sqlalchemy.orm.sessionmaker object
    :return: models.Show object
    """

    db_show = crud_show.get_show_by_title(db, title=title)
    if db_show:
        raise HTTPException(
            status_code=400,
            detail='Show already exists.',
        )

    try:
        year = int(year)
    except (TypeError, ValueError):
        raise HTTPException(
            status_code=400,
            detail="Year must be a four-digit integer."
        )

    if len(str(year)) != 4:
        raise HTTPException(
            status_code=400,
            detail="Year must be a four-digit integer."
        )

    return crud_show.create_show(db=db, title=title, year=year)


@router.get('/', response_model=list[schemas.ShowSchema])
def read_shows(
        skip: int = 0,
        limit: int = 10,
        db: Session = Depends(get_db)
) -> list:
    """Get TV shows.

    \f
    :param skip: int
    :param limit: int
    :param db: sqlalchemy.orm.sessionmaker object
    :return: list; models.Show objects
    """

    shows = crud_show.get_shows(db, skip=skip, limit=limit)

    return shows


@router.get('/{show_id}', response_model=schemas.ShowSchema)
def read_show(
        show_id: int,
        db: Session = Depends(get_db)
) -> models.Show:
    """Get TV show by ID.

    \f
    :param show_id: int
    :param db: sqlalchemy.orm.sessionmaker object
    :return: models.Show object
    """

    db_show = crud_show.get_show(db, show_id=show_id)
    if db_show is None:
        raise HTTPException(
            status_code=404,
            detail='Show not found',
        )

    return db_show
