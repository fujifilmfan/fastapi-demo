from fastapi import Depends, HTTPException
import pytest
from pytest_mock import MockerFixture
from sqlalchemy.engine.mock import MockConnection

from app.api.api_v1.endpoints import shows, users
from app.db.base import get_db
from app.models import Show, User
from app.schemas import ShowSchema, UserSchema
from .fixtures.endpoints_fixtures import (
    create_show_exceptions,
    create_show_fixtures,
    create_user_exceptions,
    create_user_fixtures,
)


_DIR = 'app.api.api_v1.endpoints'

@pytest.mark.parametrize('query_params, expected', [
    pytest.param(i['params'], i['show_obj'], id=i['id'])
    for i in create_show_fixtures
])
def test_create_show(
        query_params: dict,
        expected: Show,
        mocker: MockerFixture,
) -> None:
    """Test endpoints.shows.create_show without errors.

    :param query_params: dict
    :param expected: Show model object
    :param mocker: MockerFixture object (from pytest-mock)
    :return: None
    """

    # Arrange
    mock_get_show_by_title = mocker.patch(f"{_DIR}.shows.crud_show.get_show_by_title")
    mock_get_show_by_title.return_value = None
    mock_create_show = mocker.patch(f"{_DIR}.shows.create_show")
    mock_create_show.return_value = expected

    # Act
    actual = shows.create_show(**query_params)

    # Assert
    assert actual == mock_create_show.return_value


@pytest.mark.parametrize('query_params, show, expected', [
    pytest.param(i['params'], i['show'], i['expected'], id=i['id'])
    for i in create_show_exceptions
])
def test_create_show_exceptions(
        query_params: dict,
        show: str,
        expected: dict,
        mocker: MockerFixture,
) -> None:
    """Test endpoints.shows.create_show with errors.

    :param query_params: dict
    :param expected: dict
    :param mocker: MockerFixture object (from pytest-mock)
    :return: None
    """

    # Arrange
    mock_get_show_by_title = mocker.patch(f'{_DIR}.shows.crud_show.get_show_by_title')
    mock_get_show_by_title.return_value = show

    # Act
    with pytest.raises(HTTPException) as exc_info:
        shows.create_show(**query_params)

    # Assert
    assert exc_info.type is expected.get('exception')
    assert exc_info.value.status_code == expected.get('status_code')
    assert exc_info.value.detail == expected.get('detail')


def test_read_shows(mocker: MockerFixture):
    """Test endpoints.shows.read_shows without errors.
    """

    # Arrange
    mock_get_shows = mocker.patch(f"{_DIR}.shows.crud_show.get_shows")
    mock_get_shows.return_value = 'any show'

    # Act
    actual = shows.read_shows(skip=0, limit=10, db=get_db())

    # Assert
    assert actual == mock_get_shows.return_value


def test_read_show(mocker: MockerFixture):
    """Test endpoints.shows.read_show without errors.
    """

    # Arrange
    mock_get_show_by_title = mocker.patch(f"{_DIR}.shows.crud_show.get_show")
    mock_get_show_by_title.return_value = 'any show'

    # Act
    actual = shows.read_show(show_id=3)

    # Assert
    assert actual == mock_get_show_by_title.return_value


@pytest.mark.parametrize('query_params, expected', [
    pytest.param(i['params'], i['user_obj'], id=i['id'])
    for i in create_user_fixtures
])
def test_create_user(
        query_params: dict,
        expected: User,
        mocker: MockerFixture,
) -> None:
    """Test endpoints.shows.create_user without errors.

    :param query_params: dict
    :param expected: User model object
    :param mocker: MockerFixture object (from pytest-mock)
    :return: None
    """

    # Arrange
    mock_get_user_by_title = mocker.patch(f'{_DIR}.users.crud_user.get_user_by_email')
    mock_get_user_by_title.return_value = None
    mock_create_user = mocker.patch(f"{_DIR}.users.create_user")
    mock_create_user.return_value = expected

    # Act
    actual = users.create_user(**query_params)

    # Assert
    assert actual == mock_create_user.return_value


@pytest.mark.parametrize('query_params, user, expected', [
    pytest.param(i['params'], i['user'], i['expected'], id=i['id'])
    for i in create_user_exceptions
])
def test_create_user_exceptions(
        query_params: dict,
        user: str,
        expected: dict,
        mocker: MockerFixture,
) -> None:
    """Test endpoints.shows.create_user with errors.

    :param query_params: dict
    :param expected: dict
    :param mocker: MockerFixture object (from pytest-mock)
    :return: None
    """

    # Arrange
    mock_get_user_by_email = mocker.patch(f'{_DIR}.users.crud_user.get_user_by_email')
    mock_get_user_by_email.return_value = user

    # Act
    with pytest.raises(HTTPException) as exc_info:
        users.create_user(**query_params)

    # Assert
    assert exc_info.type is expected.get('exception')
    assert exc_info.value.status_code == expected.get('status_code')
    assert exc_info.value.detail == expected.get('detail')


def test_read_users(mocker: MockerFixture):
    """Test endpoints.shows.read_shows without errors.
    """

    # Arrange
    mock_get_users = mocker.patch(f"{_DIR}.users.crud_user.get_users")
    mock_get_users.return_value = 'any user'

    # Act
    actual = users.read_users(skip=0, limit=10, db=get_db())

    # Assert
    assert actual == mock_get_users.return_value