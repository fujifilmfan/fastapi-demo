from fastapi import HTTPException

from app import models


# ===== test fixtures for create_show() tests =====
query_params = {
    'title': 'fake_show',
    'year': 2022,
}

test_show = models.Show(title=query_params['title'], year=query_params['year'])

# ----- used in test -----
create_show_fixtures = [
    {
        'id': 'valid_params',
        'params': {
            'title': 'fake_show',
            'year': 2022,
        },
        'show_obj': test_show,
    },
    {
        'id': 'valid_params-year_string',
        'params': {
            'title': 'fake_show',
            'year': '2022',
        },
        'show_obj': test_show,
    },
]

# ===== test fixtures for create_show() exceptions tests =====
create_show_exceptions = [
    {
        'id': 'show_exists',
        'params': {'title': 'Show Title', 'year': 2022},
        'show': 'anything',
        'expected': {
            'exception': HTTPException,
            'status_code': 400,
            'detail': 'Show already exists.',
        }
    },
    {
        'id': 'year_is_invalid_string',
        'params': {'title': 'Show Title', 'year': 'Year'},
        'show': None,
        'expected': {
            'exception': HTTPException,
            'status_code': 400,
            'detail': 'Year must be a four-digit integer.',
        }
    },
    {
        'id': 'year_2_digits',
        'params': {'title': 'Show Title', 'year': 22},
        'show': None,
        'expected': {
            'exception': HTTPException,
            'status_code': 400,
            'detail': 'Year must be a four-digit integer.',
        }
    },
]

# ===== test fixtures for create_user() tests =====
query_params = {
    'email': 'fake_email@example.com',
    'name': 'Faker McFakerson',
}

test_user = models.User(email=query_params['email'], name=query_params['name'])

# ----- used in test -----
create_user_fixtures = [
    {
        'id': 'valid_params',
        'params': query_params,
        'user_obj': test_user,
    },
]

# ===== test fixtures for create_user() exceptions tests =====
create_user_exceptions = [
    {
        'id': 'user_exists',
        'params': {'email': 'totally valid email', 'name': 'me'},
        'user': 'anything',
        'expected': {
            'exception': HTTPException,
            'status_code': 400,
            'detail': 'User with email address already exists.',
        }
    },
]
