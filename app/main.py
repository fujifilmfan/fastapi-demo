from fastapi import FastAPI

from app import models
from app.db.base import engine
from app.api.api_v1.endpoints import shows, users


models.Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(shows.router)
app.include_router(users.router)


@app.get('/')
async def root():
    return {
        'message': 'Welcome to the likeable TV Shows app!',
        'version': 'v0.1.1',
    }
