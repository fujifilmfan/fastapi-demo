from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel, EmailStr


class UserBase(BaseModel):
    email: EmailStr
    name: str
    timestamp: datetime


class UserCreate(UserBase):
    pass


class User(UserBase):
    id: int

    class Config:
        orm_mode = True


class ShowBase(BaseModel):
    title: str
    year: int


class ShowCreate(ShowBase):
    pass


class Show(ShowBase):
    id: int

    class Config:
        orm_mode = True


# We have to define these to avoid a circular dependency.
class UserSchema(User):
    shows: Optional[list[Show]] = []


class ShowSchema(Show):
    users: Optional[list[User]] = []
