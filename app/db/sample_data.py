from app.crud import crud_show, crud_user
from app.db.base import SessionLocal


def load_sample_data():
    db = SessionLocal()

    # Load shows
    real_shows = [
        ('The Proud Family', 2001),
        ('Kim Possible ', 2002),
        ('Lilo & Stitch: The Series', 2003),
        ('Dave the Barbarian', 2004),
        ('Brandy & Mr. Whiskers', 2004),
        ('American Dragon: Jake Long', 2005),
        ('The Buzz on Maggie', 2005),
        ('The Emperor\'s New School', 2006),
        ('The Replacements', 2006),
        ('Phineas and Ferb', 2007),
        ('Fish Hooks', 2010),
        ('Gravity Falls', 2012),
        ('Wander Over Yonder', 2013),
        ('Star vs. the Forces of Evil', 2015),
        ('Elena of Avalor', 2016),
        ('Rapunzel\'s Tangled Adventure', 2017),
        ('DuckTales', 2018),
        ('Big Hero 6: The Series', 2018),
        ('Milo Murphy\'s Law', 2018),
        ('Star Wars Resistance', 2018),
    ]
    for show in real_shows:
        title, year = show
        crud_show.create_show(title=title, year=year, db=db)

    # Load users
    fake_users = [
        ('marilie.flatley@pagac.com', 'Marilie Flatley'),
        ('may.lockman@klein.biz', 'May Lockman'),
        ('delphine35@hotmail.com', 'Delphine'),
        ('carmen01@hauck.org', 'Carmen'),
        ('lauren14@swift.biz', 'Lauren'),
        ('isaiah12@price.org', 'Isaiah'),
        ('lang.grant@swaniawski.com', 'Lang Grant'),
        ('esmeralda.metz@gmail.com', 'Esmeralda Metz'),
        ('zander77@mclaughlin.biz', 'Zander'),
        ('kuhic.eulah@okeefe.com', 'Kuhic Eulah'),
    ]
    for user in fake_users:
        email, name = user
        crud_user.create_user(db=db, email=email, name=name)

    db.close()


if __name__ == '__main__':
    load_sample_data()
