import os
from pathlib import Path

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


DB_BASE_PATH = os.getenv("DB_BASE_PATH")
PACKAGE_DIR = Path(__file__).absolute().parent.parent
# should be: /my/path/fastapi-demo/app

base_path = DB_BASE_PATH if DB_BASE_PATH else PACKAGE_DIR
sqlite_db_path = f"sqlite:///{base_path}/app.db"

engine = create_engine(
    sqlite_db_path,
    echo=True,
    connect_args={"check_same_thread": False},
)
# Each instance of SessionLocal will be a database session.
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
