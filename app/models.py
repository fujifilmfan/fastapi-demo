from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    Table,
)
from sqlalchemy.orm import relationship

from app.db.base import Base


user_shows = Table(
    'user_shows', Base.metadata,
    Column('user_id', Integer, ForeignKey('user.id'), primary_key=True),
    Column('show_id', Integer, ForeignKey('show.id'), primary_key=True),
)


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    name = Column(String)
    timestamp = Column(DateTime)
    shows = relationship(
        'Show',
        secondary=user_shows,
        back_populates='users',
    )


class Show(Base):
    __tablename__ = 'show'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String)
    year = Column(Integer)
    users = relationship(
        'User',
        secondary=user_shows,
        back_populates='shows',
    )
