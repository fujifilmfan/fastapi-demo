# README
## Rules:
- Your solution should be able to be run locally, bonus points for using Docker.
  - We have included a basic dockerfile and file structure to get you started if you choose to use it, but it is completely OPTIONAL.
- You must write this application using either JavaScript, PHP, or Python.
- You may write this application using any open-source frameworks/libraries of your choosing.
- For simplicity, do not implement authentication or authorization.
- For simplicity, data objects may be stored in memory or a local SQLite database.
- Remember to focus on the core requirements, but feel free to go as far as you wish with your solution.
- Include a README.md file that includes the following:
	- Explains how to build, run, test the application.
	- Explains your general approach on what your solution includes.
	- Includes call-outs on incomplete functionality that could be improved with more iterations.
	- Any assumptions that you feel are necessary to point out.
	- All reference links. You may look across the internet for help (Stack Overflow) but your solution must be of your own original creation.

## Project:
- Please develop an API that manages data for two types of objects: TV Shows and Users
- Fields for TV Shows should be:
  - Title
  - Year of Release
- Fields for User should be:
  - Name
  - Email
  - Timestamp (for when Account was Created)
- Please implement an API to allow a user to like/unlike a TV Show.
- Ideally the API should include some form of documentation for the endpoints you have created (for example ReDoc or Swagger or any other docs that may built into your framework)
