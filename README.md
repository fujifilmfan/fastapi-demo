# fastapi-demo

## Contents
* [Introduction](#introduction)  
* [Running the application](#running-the-application)  
   * [Native](#native)  
   * [Docker](#docker)  
* [Using the application](#using-the-application)  
   * [Available endpoints](#available-endpoints)  
* [Testing](#testing)  
* [Design decisions](#design-decisions)  
* [To do](#to-do)  
* [Assumptions](#assumptions)  
* [References](#references)  

## Introduction
This is a simple, two-resource RESTlike API to demonstrate FastAPI.  

## Running the application

### Native
1. After cloning the repo, navigate to the root of the project in your command line app: `path/to/fastapi-demo`

2. Create a virtual environment  
    ```shell
    python -m venv .venv
    ```

3. Activate the virutal environment  
    ```shell 
    source .venv/bin/activate
    ```

4. Upgrade Pip
    ```shell
    pip install --upgrade pip
    ```

5. Install requirements
    ```shell
    pip install -r requirements-dev.txt
    ```

6. Install the application
    ```shell
    pip install -e .
    ```

7. Start the application
    ```shell
    .venv/bin/uvicorn app.main:app
    ```
    If you want the app to refresh when the code changes, add `--reload` to the startup command.  
    Startup is "noisy" due to SQLAlchemy output.  This can be changed by commenting out `echo=True` in `app/db/base.py`.  

8. Load sample data into the database (optional)
    ```shell
    python app/db/sample_data.py
    ```

### Docker
1. Pull the image from the GitLab repository
    ```shell
    docker pull registry.gitlab.com/fujifilmfan/fastapi-demo:v0.1.0
    ```

2. Start a container
    ```shell
    docker run -d -p 8000:8000 registry.gitlab.com/fujifilmfan/fastapi-demo:v0.1.0
    ```

## Using the application
Using your browser, navigate to http://127.0.0.1:8000/docs#/. There, you can add TV shows and users, look up TV shows and users by ID, and like or unlike a show.  

Alternatively, you can use cURL to interact with the API. Here's an example cURL request:  
```shell
curl -X 'PUT' 'http://127.0.0.1:8000/users/3/like_show?show_id=12' -H 'accept: application/json'
```

### Available endpoints

| Method | Path | Purpose |
| ------ | ---- | ------- |
| GET | /shows/ | Get list of all TV shows |
| POST | /shows/ | Create a new TV show entry |
| GET | /shows/{show_id} | Get one show by ID |
| GET | /users/ | Get list of all users |
| POST | /users/ | Create a new user entry |
| PUT | /users/{user_id}/like_show | Associate a show to a user (and vice versa) |
| DELETE | /users/{user_id/unlike_show} | Remove the association between a user and show |
| GET | /users/{user_id} | Get one user by ID |

## Testing
Please see `app/docs/unit-tests.md` for details about testing. tl;dr `pytest app/tests/unit`

## Design decisions
I decided to use FastAPI for this project because of its built-in support for Pydantic models, validation, and typing and because it is easy to use and blazing fast. Let's look at the project structure (created with `tree -I '__*__|.DS_Store|.git|.idea|.pytest_cache|.venv' -a`:

```shell
.
├── .coveragerc                             # specifies files and directories not to test
├── .gitignore
├── Dockerfile
├── README.md                               # you are here
├── app
│   ├── __init__.py
│   ├── api
│   │   ├── __init__.py
│   │   └── api_v1
│   │       ├── __init__.py
│   │       └── endpoints
│   │           ├── __init__.py
│   │           ├── shows.py                # /shows endpoints
│   │           └── users.py                # /users endpoints
│   ├── app.db                              # SQLite database (location is configurable by environment variable)
│   ├── crud                                # database actions
│   │   ├── __init__.py
│   │   ├── crud_show.py                    # functions for interacting with the show table
│   │   └── crud_user.py                    # functions for interacting with the user table
│   ├── db                                  # database-related files
│   │   ├── __init__.py
│   │   ├── base.py                         # configures SQLAlchemy database engine and session
│   │   └── sample_data.py                  # sample data to load into the database
│   ├── docs                                # documentation directory
│   │   └── unit-tests.md
│   ├── main.py                             # instantiates FastAPI app
│   ├── models.py                           # contains SQLAlchemy-based table definitions
│   ├── schemas.py                          # contains Pydantic models that ensure API response consistency
│   └── tests
│       ├── __init__.py
│       ├── integration                     # integration test directory (empty)
│       │   ├── __init__.py
│       │   └── fixtures
│       ├── pytest.ini                      # Pytest configuration file
│       └── unit                            # unit test directory
│           ├── __init__.py
│           ├── fixtures
│           │   └── endpoints_fixtures.py   # data for use in tests
│           └── test_endpoints.py           # tests for shows.py and users.py
├── pytest.ini                              # Pytest configuration file
├── requirements-dev.txt                    # includes testing requirements; running this also installs items in requirements.txt
├── requirements.txt
└── setup.py

```

Note that each endpoint function includes a router decorator that specifies a response model. The response models (called "schemas" due to the overlap in naming with SQLAlchemy's table models) ensure that FastAPI will always return data to the client in a consistent format.  

I chose to use SQLAlchemy and SQLite primarily due to the limited options given in the assignment. I would have been more comfortable with Postgres and saving user and show relationships as JSON (instead of using an assocation table), but that would also have taken more time. I have not used SQLAlchemy or SQLite much, so I think most of my configuration and troubleshooting time was spent on database-related parts of the app (`models.py`, the CRUD actions, and `sample_data.py`).  

Instead of using an automatically-maintained association table, I also thought of making a table like the following:  

| user_id | show_id | liked |
| ------- | ------- | ----- |
| 2 | 3 | True |
| 1 | 3 | True |
| 1 | 2 | True |
| 2 | 2 | False |

With this schema, `False` implies that the user has unliked a previously liked show. The lack of a row implies that the user has never liked a particular show. For example, neither user in the above table has ever liked show 1. I like this idea because this table can be more easily modified directly by the API and it carries more information (see [To do](#to-do) for some use cases).

## To do
* add a field to show objects for total number of likes
* remove user objects from show objects - maybe the show could just have the IDs of users if any user information at all
* add ability for a user to see all shows they have not yet liked
* store data about unliking a show so that there are three categories of show to a user: liked, unliked, and not liked
* add an endpoint or at least a function for clearing data from the database

## Assumptions
I assumed that when a user likes or unlikes a TV show, that information should be stored in the database. I also thought it would be useful for a show to be aware of how many likes it has.  


## References
* [FastAPI](https://fastapi.tiangolo.com/)
* [Production app structure and API versioning](https://christophergs.com/tutorials/ultimate-fastapi-tutorial-pt-8-project-structure-api-versioning/)
* [SO: What are the best practices for structuring a FastAPI project?](https://stackoverflow.com/questions/64943693/what-are-the-best-practices-for-structuring-a-fastapi-project)
* [SO: Relative imports in Python 3](https://stackoverflow.com/questions/16981921/relative-imports-in-python-3)
* [SO: Split requirements files in pip](https://stackoverflow.com/questions/11704287/split-requirements-files-in-pip)
* [FastAPI: SQL (Relational) Databases](https://fastapi.tiangolo.com/tutorial/sql-databases/?h=sqli)
* [SQLAlchemy: Many To Many](https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html#many-to-many)
* [SQLAlchemy: backref](https://docs.sqlalchemy.org/en/14/orm/relationship_api.html#sqlalchemy.orm.relationship.params.backref)
* [SQLAlchemy: Generic Types](https://docs.sqlalchemy.org/en/14/core/type_basics.html#generic-types)
* [Many-To-Many Relationships In FastAPI](https://www.gormanalysis.com/blog/many-to-many-relationships-in-fastapi/)
* [SO: SQLAlchemy ORM - many to many relationship query with junction table attribute](https://stackoverflow.com/questions/70321045/sqlalchemy-orm-many-to-many-relationship-query-with-junction-table-attribute/70352931#70352931)

* [SO: SQLAlchemy delete association objects](https://stackoverflow.com/questions/19736334/sqlalchemy-delete-association-objects)
* [SO: Remove a relation many-to-many (association object) on Sqlalchemy](https://stackoverflow.com/questions/24477806/remove-a-relation-many-to-many-association-object-on-sqlalchemy)
* [SQLAlchemy ORM - Updating Objects](https://www.tutorialspoint.com/sqlalchemy/sqlalchemy_orm_updating_objects.htm)
* [Pydantic Field Types](https://pydantic-docs.helpmanual.io/usage/types/)
* [MDN: HTTP response status codes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)
* [How to add time onto a datetime object in Python](https://www.adamsmith.haus/python/answers/how-to-add-time-onto-a-datetime-object-in-python)
* [Python: datetime — Basic date and time types](https://docs.python.org/3/library/datetime.html)
* [Wikipedia: List of programs broadcast by Disney Channel](https://en.wikipedia.org/wiki/List_of_programs_broadcast_by_Disney_Channel)
* [Create List of Fake Emails](https://fauxid.com/tools/fake-credit-card-generator)
* [Install Docker Desktop on Mac](https://docs.docker.com/desktop/mac/install/)
* [Docker: python](https://hub.docker.com/_/python)
* [Dockerfile reference: ENV](https://docs.docker.com/engine/reference/builder/#env)
* [Dockerfile reference: WORKDIR](https://docs.docker.com/engine/reference/builder/#workdir)
* [SO: Why is docker build not showing any output from commands?](https://stackoverflow.com/questions/64804749/why-is-docker-build-not-showing-any-output-from-commands)
* [FASTAPI- Docker (uvicorn not found)](https://www.reddit.com/r/FastAPI/comments/rz1avi/fastapi_docker/)
* [--cov don't include recursively folders and files](https://github.com/pytest-dev/pytest-cov/issues/499)
  `pytest-cov` only looks in directories with an `__init__.py`!
* [ZSH - output whole history?](https://superuser.com/questions/232457/zsh-output-whole-history)

